import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import reducers from './reducers/index';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import App from './components/app';
import { AUTH_USER } from './actions/types';
import { getItem } from './localstorage-utils';

import 'normalize.css/normalize.css';

const createStoreWithMiddleware = applyMiddleware (reduxThunk) (createStore);
const store = createStoreWithMiddleware (reducers); 

const token = getItem ('token');

if (token) {
    store.dispatch ({ type: AUTH_USER })
}

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
, document.getElementById('root'));
registerServiceWorker();
