import axios from "axios";
import { LOCAL_URL, API_URL, errorHandler } from "./index";
import { AUTH_ERROR, AUTH_USER, UNAUTH_USER, ADMIN_USER_LIST } from "./types";
import { setItem, getItem } from "../localstorage-utils";

export function loginUser({ email, password }) {
  return function(dispatch) {
    axios
      .post(`${API_URL}/auth/login`, { email, password })
      .then(response => {
        setItem("user", response.data.user);
        setItem("token", response.data.token);
        dispatch({ type: AUTH_USER });
        window.location.href = `${LOCAL_URL}`;
      })
      .catch(error => {
        errorHandler(dispatch, error, AUTH_ERROR);
      });
  };
}

export function logoutUser(error) {
  return function(dispatch) {
    dispatch({ type: UNAUTH_USER, payload: error || "" });
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    window.location.href = `${LOCAL_URL}logout`;
  };
}

export function registerUser({ email, password, name }) {
  return function(dispatch) {
    axios
      .post(`${API_URL}/auth/register`, { email, password, name })
      .then(response => {
        console.log(response);
        setItem("user", response.data.user);
        setItem("token", response.data.token);
        dispatch({ type: AUTH_USER });
        window.location.href = `${LOCAL_URL}`;
      })
      .catch(error => {
        errorHandler(dispatch, error, AUTH_ERROR);
      });
  };
}

export function listUsers() {
  return function(dispatch) {
    const config = {
      headers: {
        Authorization: `${getItem("token").tokenType} ${
          getItem("token").accessToken
        }`
      }
    };

    axios
      .get(`${API_URL}/users`, config)
      .then(response => {
        dispatch({ type: ADMIN_USER_LIST, payload: response.data });
      })
      .catch(err => {
        errorHandler(dispatch, err, AUTH_ERROR);
      });
  };
}
