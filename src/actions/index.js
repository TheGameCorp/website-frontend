export const API_URL = "http://thechronium.go.ro:4000/v1";
export const LOCAL_URL = "http://thechronium.go.ro:3000/";

export function errorHandler(dispatch, error, type) {
  let errorMessage = error.response ? error.response.data : error;

  if (error.status === 401 || error.response.status === 401) {
    errorMessage = "Incorrect email or password.";
  }

  if (error.status === 403 || error.response.status === 403) {
    errorMessage = "Unauthorized.";
  }

  if (error.status === 409 || error.response.status === 409) {
    errorMessage = "Email already in use";
  }

  if (error.status === 400 || error.response.status === 400) {
    errorMessage = "Password is too short.";
  }

  dispatch({
    type,
    payload: errorMessage
  });
}
