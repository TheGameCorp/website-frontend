import React from 'react';
import { Card, Elevation, Text, Icon, Button } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

const contractListStyles = {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
}

const contractCardStyles = {
    maxWidth: "30%",
    minWidth: "150px",
    flex: "1",
    margin: "5px",
}

const contractTextStyles = {
    textAlign: "justify",
    whiteSpace: "pre-line",
    display: "inline-block",
}

class Contracts extends React.Component {
    render () {
        const { contracts } = this.props;
        const contractIDs = Object.keys (contracts);

        return (
            <div>
                <h1 style={{textAlign: 'center'}}>Contracts</h1>

                <div style={contractListStyles}>
                    {contractIDs.map ((id) => {
                        const contract = contracts[id]
                        return (
                            <Card key={id} elevation={Elevation.TWO} style={contractCardStyles}>
                                <h4 style={{textAlign: 'center'}}>{contract.title}</h4>
                                <br />
                                <Text><Icon icon={IconNames.INFO_SIGN} /> Information: </Text>
                                <br />
                                <div style={contractTextStyles}>{contract.description}</div>
                                <hr />
                                <Text><Icon icon={IconNames.LIGHTBULB} /> Lore: </Text>
                                <br />
                                <blockquote><div style={contractTextStyles}>{contract.quote}</div></blockquote>
                                <br />
                                <Button icon={IconNames.TICK} style={{float: 'left'}} text="Accept" />
                                <Button rightIcon={IconNames.CROSS} style={{float: 'right'}} text="Decline" />
                            </Card>
                        )
                    })}
                </div>
            </div>
        );
    }
}

export default Contracts;
