import { AUTH_USER, UNAUTH_USER, AUTH_ERROR, AUTH_CLEAR_ERROR } from '../actions/types';

const INITIAL_STATE = { error: '', message: '', content: '', autenticated: false };

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case AUTH_USER:
            return { ...state, error: '', message: '', authenticated: true };
        case UNAUTH_USER:
            return { ...state, autenticated: false, error: action.payload };
        case AUTH_ERROR:
            return { ...state, error: action.payload };
        case AUTH_CLEAR_ERROR:
            return { ...state, error: '' }
        default:
            return state;
    }
}
