import { combineReducers } from "redux";
import authReducer from "./auth_reducer";
import adminReducer from "./admin_reducer";
import { reducer as formReducer } from "redux-form";

const rootReducer = combineReducers({
  auth: authReducer,
  form: formReducer,
  admin: adminReducer
});

export default rootReducer;
