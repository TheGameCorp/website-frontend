import { ADMIN_USER_LIST } from "../actions/types";

const INITIAL_STATE = {
  users: []
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ADMIN_USER_LIST:
      return { ...state, users: action.payload };
    default:
      return state;
  }
}
