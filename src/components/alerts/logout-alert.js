import React, { Component } from "react";
import { Button, Classes, Alert, Intent } from "@blueprintjs/core";
import { withRouter } from "react-router";

class LogoutAlert extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };

    this.toggleAlert = this.toggleAlert.bind(this);
  }

  toggleAlert() {
    this.setState({ isLogoutOpen: !this.state.isLogoutOpen });
  }

  render() {
    const {
      icon,
      titleText,
      confirmText,
      cancelText,
      history,
      body
    } = this.props;

    return (
      <div>
        <Button
          className={Classes.MINIMAL}
          icon={icon}
          text={titleText}
          onClick={this.toggleAlert}
        />
        <Alert
          canOutsideClickCancel={true}
          confirmButtonText={confirmText}
          cancelButtonText={cancelText}
          icon={icon}
          intent={Intent.WARNING}
          isOpen={this.state.isLogoutOpen}
          onConfirm={() => {
            this.toggleAlert();
            history.push("/logout");
          }}
          onCancel={() => {
            this.toggleAlert();
          }}
        >
          {body}
        </Alert>
      </div>
    );
  }
}

export default withRouter(LogoutAlert);
