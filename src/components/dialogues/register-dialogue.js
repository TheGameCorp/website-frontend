import React from "react";
import {
  ControlGroup,
  InputGroup,
  FormGroup,
  Tooltip,
  Intent,
  Button,
  Alert
} from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import { registerUser } from "../../actions/auth";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import { AUTH_CLEAR_ERROR } from "../../actions/types";

const form = reduxForm({
  form: "register"
});

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showPassword: false
    };

    this.handleLockClick = this.handleLockClick.bind(this);
  }

  handleFormSubmit() {
    this.props.registerUser({
      email: this.emailInput.value,
      password: this.passwordInput.value,
      name: this.nickInput.value
    });
  }

  handleLockClick() {
    this.setState({ showPassword: !this.state.showPassword });
  }

  render() {
    const { showPassword } = this.state;
    const { handleSubmit, dispatch, errorMessage } = this.props;

    const lockIcon = showPassword ? IconNames.EYE_OPEN : IconNames.EYE_OFF;

    const lockButton = (
      <Tooltip content={`${showPassword ? "Hide" : "Show"} Password`}>
        <Button
          icon={lockIcon}
          intent={Intent.WARNING}
          onClick={this.handleLockClick}
        />
      </Tooltip>
    );

    return (
      <div>
        <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
          <div className="pt-dialog-body">
            <FormGroup>
              <ControlGroup vertical={true} fill={true}>
                <InputGroup
                  inputRef={input => (this.emailInput = input)}
                  leftIcon={IconNames.ENVELOPE}
                  placeholder="Email"
                  type="email"
                  large={true}
                  required={true}
                />
                <InputGroup
                  inputRef={input => (this.passwordInput = input)}
                  leftIcon={IconNames.KEY}
                  rightElement={lockButton}
                  placeholder="Password"
                  type={showPassword ? "text" : "password"}
                  large={true}
                  required={true}
                  pattern=".{6,128}"
                  title="6 to 128 characters"
                />
              </ControlGroup>
            </FormGroup>
            <FormGroup>
              <ControlGroup vertical={true} fill={true}>
                <InputGroup
                  inputRef={input => (this.nickInput = input)}
                  leftIcon={IconNames.PERSON}
                  placeholder="Nickname"
                  type="name"
                  large={true}
                  required={true}
                />
              </ControlGroup>
            </FormGroup>
          </div>
          <div className="pt-dialog-footer">
            <div className="pt-dialog-footer-actions">
              <Button intent={Intent.PRIMARY} text="Register" type="submit" />
            </div>
          </div>
        </form>
        <Alert
          icon={IconNames.ERROR}
          intent={Intent.DANGER}
          isOpen={errorMessage}
          onConfirm={() =>
            dispatch({
              type: AUTH_CLEAR_ERROR
            })
          }
        >
          {" "}
          {errorMessage}{" "}
        </Alert>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
    authenticated: state.auth.authenticated
  };
}

export default connect(mapStateToProps, { registerUser })(form(Register));
