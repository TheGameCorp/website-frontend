import React, { Component } from "react";
import {
  ControlGroup,
  InputGroup,
  FormGroup,
  Tooltip,
  Intent,
  Button,
  Alert
} from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import { loginUser } from "../../actions/auth";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import { AUTH_CLEAR_ERROR } from "../../actions/types";

const form = reduxForm({
  form: "login"
});

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPassword: false
    };

    this.handleLockClick = this.handleLockClick.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleFormSubmit(formProps) {
    this.props.loginUser({
      email: this.emailInput.value,
      password: this.passwordInput.value
    });
  }

  handleLockClick() {
    this.setState({ showPassword: !this.state.showPassword });
  }

  render() {
    const { showPassword } = this.state;
    const { handleSubmit, dispatch, errorMessage } = this.props;

    const lockIcon = showPassword ? IconNames.EYE_OPEN : IconNames.EYE_OFF;

    const lockButton = (
      <Tooltip content={`${showPassword ? "Hide" : "Show"} Password`}>
        <Button
          icon={lockIcon}
          intent={Intent.WARNING}
          onClick={this.handleLockClick}
        />
      </Tooltip>
    );

    return (
      <div>
        <form onSubmit={handleSubmit(this.handleFormSubmit)}>
          <div className="pt-dialog-body">
            <FormGroup>
              <ControlGroup vertical={true} fill={true}>
                <InputGroup
                  inputRef={input => (this.emailInput = input)}
                  leftIcon={IconNames.ENVELOPE}
                  placeholder="Email"
                  type="email"
                  large={true}
                  required={true}
                />
                <InputGroup
                  inputRef={input => (this.passwordInput = input)}
                  id="password"
                  leftIcon={IconNames.KEY}
                  rightElement={lockButton}
                  placeholder="Password"
                  type={showPassword ? "text" : "password"}
                  large={true}
                  required={true}
                />
              </ControlGroup>
            </FormGroup>
          </div>
          <div className="pt-dialog-footer">
            <div className="pt-dialog-footer-actions">
              <Button text="Forgot Password" />
              <Button intent={Intent.PRIMARY} text="Log In" type="submit" />
            </div>
          </div>
        </form>

        <Alert
          icon={IconNames.ERROR}
          intent={Intent.DANGER}
          isOpen={errorMessage}
          onConfirm={() =>
            dispatch({
              type: AUTH_CLEAR_ERROR
            })
          }
        >
          {" "}
          {errorMessage}{" "}
        </Alert>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
    authenticated: state.auth.authenticated
  };
}

export default connect(mapStateToProps, { loginUser })(form(Login));
