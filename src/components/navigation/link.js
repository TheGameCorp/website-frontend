import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Icon, Classes } from "@blueprintjs/core";

class Link extends Component {
  render() {
    const { to, exact, icon, text } = this.props;

    return (
      <NavLink
        exact={exact}
        to={to}
        activeClassName={Classes.ACTIVE}
        className="pt-minimal pt-button"
      >
        <Icon icon={icon} />
        <span>{text}</span>
      </NavLink>
    );
  }
}

export default Link;
