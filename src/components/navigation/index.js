import React from "react";
import {
  Button,
  Navbar,
  NavbarGroup,
  NavbarHeading,
  NavbarDivider,
  Alignment,
  Classes,
  FocusStyleManager,
  Dialog,
} from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import Register from "../dialogues/register-dialogue";
import Login from "../dialogues/login-dialogue";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { getItem } from "../../localstorage-utils";
import Authenticated from "../authenticated";
import Link from "./link";
import LogoutAlert from "../alerts/logout-alert";

FocusStyleManager.onlyShowFocusOnTabs();

class Nav extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isRegisterOpen: false,
      isLoginOpen: false,
      isLogoutOpen: false
    };

    this.toggleRegister = this.toggleRegister.bind(this);
    this.toggleLogin = this.toggleLogin.bind(this);
    this.toggleLogout = this.toggleLogout.bind(this);
  }

  toggleRegister() {
    this.setState({ isRegisterOpen: !this.state.isRegisterOpen });
  }

  toggleLogin() {
    this.setState({ isLoginOpen: !this.state.isLoginOpen });
  }

  toggleLogout() {
    this.setState({ isLogoutOpen: !this.state.isLogoutOpen });
  }

  render() {
    const { name } = localStorage.user ? getItem("user") : "";

    return (
      <Navbar>
        <NavbarGroup align={Alignment.LEFT}>
          <NavbarHeading>The Game Panel</NavbarHeading>
          <NavbarDivider />
          <Link exact to="/" icon={IconNames.HOME} text="Home" />
          <Authenticated
            isAuth={
              <Link
                exact
                to="/contracts"
                icon={IconNames.DOCUMENT}
                text="Contracts"
              />
            }
          />
          <Authenticated
            isAuth={
              <Link
                exact
                to="/moxie"
                icon={IconNames.FLOPPY_DISK}
                text="Moxie"
              />
            }
          />
        </NavbarGroup>
        <Authenticated
          isAuth={
            <NavbarGroup align={Alignment.RIGHT}>
              <NavbarHeading>Welcome {name}</NavbarHeading>
              <NavbarDivider />
              <LogoutAlert
                icon="log-out"
                titleText="Log Out"
                confirmText="I'm sure"
                cancelText="Never mind"
                body="Are you sure you want to log out?"
              />
            </NavbarGroup>
          }
          notAuth={
            <NavbarGroup align={Alignment.RIGHT}>
              <Button
                className={Classes.MINIMAL}
                icon="log-in"
                text="Log In"
                onClick={this.toggleLogin}
              />
              <Dialog
                icon={IconNames.USER}
                isOpen={this.state.isLoginOpen}
                onClose={this.toggleLogin}
                title="Log In"
              >
                <Login />
              </Dialog>
              <NavbarDivider />
              <Button
                className={Classes.MINIMAL}
                icon="person"
                text="Register"
                onClick={this.toggleRegister}
              />
              <Dialog
                icon={IconNames.PERSON}
                isOpen={this.state.isRegisterOpen}
                onClose={this.toggleRegister}
                title="Register"
              >
                <Register />
              </Dialog>
            </NavbarGroup>
          }
        />
      </Navbar>
    );
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated
  };
}

export default withRouter(connect(mapStateToProps, null)(Nav));
