import React, { Component } from "react";
import { connect } from "react-redux";
import { getItem } from "../localstorage-utils.js";

class Authenticated extends Component {
  render() {
    const { authenticated, admin } = this.props;
    const { role } = localStorage.user ? getItem("user") : "";

    if (authenticated && role === "admin")
      return this.props.isAdmin || this.props.isAuth || <div />;
    else if (authenticated && role === "user" && !admin)
      return this.props.isAuth || <div />;

    return this.props.notAuth || <div />;
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated
  };
}

export default connect(mapStateToProps)(Authenticated);
