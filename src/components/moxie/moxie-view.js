import React, { Component } from "react"
import Websocket from "react-websocket"
import Sockette from "sockette"
import { getItem } from "../../localstorage-utils"

export default class MoxieView extends Component {
  constructor(props) {
    super(props)

    this.state = {
      connected: false,
    }

    // Open websocket connection
    this.ws = new Sockette("ws://thechronium.go.ro:5000", {
      timeout: 5e3,
      maxAttempts: 10,
      onopen: e => {
        this.ws.send(getItem("token").accessToken)
        this.setState({
          ...this.state,
          connected: true,
        })
        console.log("Connected!", e)
      },
      onmessage: e => console.log("Received:", e),
      onreconnect: e => console.log("Reconnecting...", e),
      onmaximum: e => console.log("Stop Attempting!", e),
      onclose: e => console.log("Closed!", e),
      onerror: e => console.log("Error:", e)
    });
  }

  componentWillUnmount() {

    // Close websocket connection
    this.ws.close()
  }

  render() {
    return (
      <div>
        <span hidden={!this.state.connected}>Connected.</span>
      </div>
    )
  }
}