import React, { Component } from "react";
import { NonIdealState } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import styled from "styled-components";

const CenterAlign = styled.div`
  height: 100vh;
  margin-top: -70px;
  padding-top: 70px;
`;

export default class UnauthorizedPage extends Component {
  render() {
    return (
      <CenterAlign>
        <NonIdealState
          title="Unauthorized"
          description="You do not have the required permissions to view this page."
          visual={IconNames.CROSS}
        />
      </CenterAlign>
    );
  }
}
