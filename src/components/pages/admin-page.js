import React, { Component } from "react";
import { connect } from "react-redux";
import { AUTH_CLEAR_ERROR } from "../../actions/types";
import { listUsers } from "../../actions/auth";
import { Intent, Alert, Card, Elevation, Label, Tag } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import { reduxForm } from "redux-form";
import styled from "styled-components";

const form = reduxForm({
  form: "login"
});

class AdminPage extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.listUsers();
  }

  render() {
    const { dispatch, errorMessage, users } = this.props;

    console.log(users);

    return (
      <div>
        <h1>Admin Page</h1>
        <div>
          {users.map(user => {
            return (
              <Card interactive={true} elevation={Elevation.TWO} key={user.id}>
                Nickname: {user.name}
                <br />
                Roles: <Tag className="pt-minimal">{user.role}</Tag>
              </Card>
            );
          })}
        </div>
        <Alert
          icon={IconNames.ERROR}
          intent={Intent.DANGER}
          isOpen={errorMessage}
          onConfirm={() =>
            dispatch({
              type: AUTH_CLEAR_ERROR
            })
          }
        >
          You don't have the required permission
        </Alert>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
    authenticated: state.auth.authenticated,
    users: state.admin.users
  };
}

export default connect(mapStateToProps, { listUsers })(form(AdminPage));
