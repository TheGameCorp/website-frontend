import React, { Component } from "react";
import * as actions from "../../actions/auth";
import { connect } from "react-redux";
import { withRouter } from "react-router";

class LogoutPage extends Component {
  componentWillMount() {
    if (this.props.authenticated) this.props.logoutUser();
  }

  render() {
    return <h1> Good bye. It was nice having you around. </h1>;
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated
  };
}

export default withRouter(connect(mapStateToProps, actions)(LogoutPage));
