import React, { Component } from "react";
import Websocket from "react-websocket";
import Sockette from "sockette";
import { getItem } from "../../localstorage-utils";
import MoxieView from "../moxie/moxie-view";

export default class MoxiePage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h1>Welcome to Moxie!</h1>
        <p>This shall be the moxie interface that runs via websocks.</p>
        <MoxieView />
      </div>
    );
  }
}
