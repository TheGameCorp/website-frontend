import React, { Component } from "react";

class HomePage extends Component {
  render() {
    return (
      <div>
        <h1>This is the home page.</h1>
      </div>
    );
  }
}

export default HomePage;
