import React, { Component } from "react";
import Nav from "./navigation";
import { Switch, Route } from "react-router";
import HomePage from "./pages/home-page";
import LogoutPage from "./pages/logout-page";
import { withRouter } from "react-router";
import LoginPage from "./pages/login-page";
import PrivateRoute from "./private-route.js";
import MoxiePage from "./pages/moxie-page";
import ProfilePage from "./pages/profile-page";
import AdminPage from "./pages/admin-page";
import UnauthorizedPage from "./pages/unauthorized-page";

class App extends Component {
  render() {
    return (
      <div>
        <Nav />
        <Switch className="pt-app">
          <Route exact path="/" component={HomePage} />
          <Route path="/login" component={LoginPage} />
          <Route path="/logout" component={LogoutPage} />
          <PrivateRoute path="/profile" component={ProfilePage} />
          <PrivateRoute path="/moxie" component={MoxiePage} />
          <PrivateRoute path="/unauthorized" component={UnauthorizedPage} />
          <PrivateRoute admin path="/admin" component={AdminPage} />
          <PrivateRoute
            path="/contracts"
            component={() => <h1>Contracts</h1>}
          />
        </Switch>
      </div>
    );
  }
}

export default withRouter(App);
