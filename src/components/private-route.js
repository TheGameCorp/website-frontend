import React from "react";
import Authenticated from "./authenticated";
import { Route, Redirect, withRouter } from "react-router-dom";
import { connect } from "react-redux";

const PrivateRoute = ({ component: Component, admin, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <Authenticated
        admin={admin}
        isAuth={!admin && <Component {...props} />}
        isAdmin={admin && <Component {...props} />}
        notAuth={
          !admin ? <Redirect to="/login" /> : <Redirect to="/unauthorized" />
        }
      />
    )}
  />
);

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated
  };
}

export default withRouter(connect(mapStateToProps)(PrivateRoute));
